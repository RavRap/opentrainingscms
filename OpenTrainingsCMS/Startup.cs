﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OpenTrainingsCMS.Startup))]
namespace OpenTrainingsCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
