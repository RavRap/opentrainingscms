using Microsoft.Practices.Unity;
using OpenTrainingsCMS.Application;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Mvc;
using Unity.Mvc5;

namespace OpenTrainingsCMS
{
    public static class UnityConfig
    {
        //public static void RegisterComponents()
        //{
        //    var container = new UnityContainer();                                    
        //    GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        //}

        private static readonly UnityDependencyResolverApi unityDependencyResolver;

        static UnityConfig()
        {
            var container = new OpenTrainingsCmsAppContainer();
            unityDependencyResolver = new UnityDependencyResolverApi(container);
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }

        public static UnityDependencyResolverApi UnityDependencyResolver
        {
            get { return unityDependencyResolver; }
        }

        public static void RegisterComponents()
        {
            DependencyResolver.SetResolver(UnityDependencyResolver);            
        }
    }

    public class UnityDependencyResolverApi : UnityDependencyResolver, System.Web.Http.Dependencies.IDependencyResolver
    {
        private readonly IUnityContainer _container;

        public UnityDependencyResolverApi(IUnityContainer container) : base(container)
        {
            _container = container;
        }

        public void Dispose()
        {
            _container.Dispose();
        }

        public IDependencyScope BeginScope()
        {
            var child = _container.CreateChildContainer();
            return new UnityDependencyResolverApi(child);
        }
    }    
}