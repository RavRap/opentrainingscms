﻿using Microsoft.Practices.Unity;
using OpenTrainingsCMS.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity.Mvc5;

namespace OpenTrainingsCMS
{
    public class DependencyResolverConfig
    {
        static IUnityContainer Container
        {
            get { return _container ?? (_container = new OpenTrainingsCmsAppContainer()); }
        }
        private static IUnityContainer _container;

        public static IUnityContainer Initialize()
        {
            DependencyResolver.SetResolver(new UnityDependencyResolver(Container));

            // Session Value Provider
            //Container.RegisterType<SessionValueProvider>(new InjectionConstructor(new HttpSessionDictionary()));

            return Container;
        }

        public static T ResolveWithCurrent<T>()
        {
            return Container.Resolve<T>();
        }

        public static object ResolveWithCurrent(Type t)
        {
            return Container.Resolve(t);
        }
    }
}