/// <reference path="../typings/_all.ts" />
ko.validation.init({
    insertMessages: false,
    decorateElement: true,
    decorateElementOnModified: true,
    errorElementClass: 'error',
    errorClass: 'error',
    messagesOnModified: false,
    decorateInputElement: true
}, true);
var OpenTrainingsCMS;
(function (OpenTrainingsCMS) {
    var PopupHelper = (function () {
        function PopupHelper(_onClose) {
            var _this = this;
            this._onClose = _onClose;
            this.onPopupOpen = function () {
                _this.openedFromPosition = $(window).scrollTop();
                $('.' + PopupHelper.MENU_CLASS).hide();
            };
            this.onPopupClose = function () {
                if (_this._onClose)
                    _this._onClose();
                $('.' + PopupHelper.MENU_CLASS).show();
                var vm = _this;
                setTimeout(function () {
                    $(window).scrollTop(vm.openedFromPosition);
                    menu_focus($("li[data-slide=1]"), 1);
                }, 25);
            };
            this.openedFromPosition = undefined;
        }
        PopupHelper.MENU_CLASS = "navbar";
        return PopupHelper;
    })();
    //http://tympanus.net/codrops/2012/06/05/fullscreen-slit-slider-with-jquery-and-css3/
    var Carousel = (function () {
        function Carousel() {
            var _this = this;
            this.setupItems = function () {
                _this.items = ko.observableArray();
                var self = _this;
                var i = 0;
                var numberPattern = /\d+/g;
                $.each(OpenTrainingsCMS.trainings, function (indexInArray, valueOfElement) {
                    var numbers = valueOfElement.Date.match(numberPattern);
                    valueOfElement.Date = (new Date(parseInt(numbers[0]))).toISOString();
                    _this.items.push($.extend({}, valueOfElement, self._animations[i]));
                    if (i >= self._animations.length - 1)
                        i = 0;
                    else
                        i++;
                });
            };
            this.setupAnimations = function () {
                _this._animations = [
                    {
                        orientation: 'horizontal',
                        slice1_rotation: -25,
                        slice2_rotation: -25,
                        slice1_scale: 2,
                        slice2_scale: 2,
                        _class: 'bg-img bg-img-1'
                    },
                    {
                        orientation: 'vertical',
                        slice1_rotation: 10,
                        slice2_rotation: -15,
                        slice1_scale: 1.5,
                        slice2_scale: 1.5,
                        _class: 'bg-img bg-img-2'
                    },
                    {
                        orientation: 'horizontal',
                        slice1_rotation: 3,
                        slice2_rotation: 3,
                        slice1_scale: 2,
                        slice2_scale: 1,
                        _class: 'bg-img bg-img-3'
                    },
                    {
                        orientation: 'vertical',
                        slice1_rotation: -5,
                        slice2_rotation: 25,
                        slice1_scale: 2,
                        slice2_scale: 1,
                        _class: 'bg-img bg-img-4'
                    },
                    {
                        orientation: 'horizontal',
                        slice1_rotation: -5,
                        slice2_rotation: 10,
                        slice1_scale: 2,
                        slice2_scale: 1,
                        _class: 'bg-img bg-img-5'
                    }
                ];
            };
            this.setupAnimations();
            this.setupItems();
        }
        return Carousel;
    })();
    var App = (function () {
        function App() {
            var _this = this;
            this.setupValidation = function () {
                _this.user.FirstName.extend({ required: { params: true, message: 'First name is required' } });
                _this.user.LastName.extend({ required: { params: true, message: 'Last name is required.' } });
                _this.user.Email.extend({ required: { params: true, message: 'Email is required.' } })
                    .extend({ email: { params: true, message: 'Email is required.' } });
                _this.user.Firm.extend({ required: { params: true, message: 'Firm is required.' } });
                _this.user.Phone.extend({ required: { params: true, message: 'Phone is required.' } });
                _this.user.Position.extend({ required: { params: true, message: 'Position is required.' } });
                _this.agreedToTerms.extend({
                    validation: [
                        {
                            validator: function (value) { return value === true; },
                            message: function () { return "You must agree to terms."; }
                        }]
                });
                _this.showErrors = ko.observable(false);
                _this.validationModel = ko.validatedObservable({
                    FirstName: _this.user.FirstName,
                    LastName: _this.user.LastName,
                    Email: _this.user.Email,
                    Firm: _this.user.Firm,
                    Phone: _this.user.Phone,
                    Position: _this.user.Position,
                    AgreedToTerms: _this.agreedToTerms
                });
                _this.validationModel.errors.showAllMessages(false);
            };
            this.startSigningUp = function (training) {
                _this._chosenTraining.update(training);
                _this.showErrors(false);
            };
            this.signUp = function () {
                _this.showErrors(true);
                _this.validationModel.errors.showAllMessages(true);
                if (_this.validationModel.isValid()) {
                    $.ajax({
                        type: 'POST',
                        url: '/Home/SignToTraining',
                        data: {
                            trainingId: _this._chosenTraining.Id,
                            user: _this.user.parse()
                        },
                        success: function (data, textStatus, jqXHR) {
                            $("button.btn.md-close").click(); //not sweet, but well I work here with beautiful plguging with ugly code
                            menu_focus($("li[data-slide=1]"), 1);
                        }
                    });
                }
                ;
            };
            this.popupHelper = new PopupHelper(function () { return _this.user.reset(); });
            this.carousel = new Carousel();
            this._chosenTraining = new OpenTrainingsCMS.Dto.TrainingDto();
            this.signUpHeader = ko.computed({
                owner: this,
                read: function () {
                    return 'Training: ' + _this._chosenTraining.Title();
                }
            });
            this.user = new OpenTrainingsCMS.Dto.UserDto();
            this.agreedToTerms = ko.observable(false);
            this.canSign = ko.observable(true);
            this.user.Email.subscribe(function (newValue) {
                var self = _this;
                $.ajax({
                    type: 'POST',
                    url: '/Home/GetUser',
                    data: {
                        user: _this.user.parse()
                    },
                    //contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        if (data !== false) {
                            var matched = $.grep(data.TrainingIds, function (elementOfArray, indexInArray) {
                                return elementOfArray == self._chosenTraining.Id;
                            });
                            if (matched.length > 0) {
                                // user is already signed to choosen training
                                self.canSign(false);
                            }
                            else {
                                // found user not signed to choosen training, fill other input fields
                                self.canSign(true);
                                self.user.update(data);
                            }
                        }
                        else {
                            self.canSign(true);
                        }
                        ;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            });
            this.setupValidation();
        }
        return App;
    })();
    OpenTrainingsCMS.App = App;
    OpenTrainingsCMS.app = new OpenTrainingsCMS.App();
    ko.applyBindings(OpenTrainingsCMS.app);
})(OpenTrainingsCMS || (OpenTrainingsCMS = {}));
//# sourceMappingURL=app.js.map