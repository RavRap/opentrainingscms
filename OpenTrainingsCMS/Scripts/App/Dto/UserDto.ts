﻿module OpenTrainingsCMS.Dto {

    export class UserDto {        

        public FirstName: KnockoutObservable<string>;
        public LastName: KnockoutObservable<Date>;
        public Phone: KnockoutObservable<string>;
        public Email: KnockoutObservable<Date>;
        public Position: KnockoutObservable<string>;
        public Firm: KnockoutObservable<string>;

        constructor() {
            
            this.FirstName = ko.observable(undefined);
            this.LastName = ko.observable(undefined);
            this.Phone = ko.observable(undefined);
            this.Email = ko.observable(undefined);
            this.Position = ko.observable(undefined);
            this.Firm = ko.observable(undefined);
        }

        update = (source: any): void => {
            this.FirstName(source.FirstName);
            this.LastName(source.LastName);
            this.Phone(source.Phone);
            this.Email(source.Email);
            this.Position(source.Position);
            this.Firm(source.Firm);
        }

        parse = (): any => {
            return {                
                FirstName: this.FirstName(),
                LastName: this.LastName(),
                Phone: this.Phone(),
                Email: this.Email(),
                Position: this.Position(),
                Firm: this.Firm()
            };
        }

        reset = (): void => {
            this.FirstName(undefined);
            this.LastName(undefined);
            this.Phone(undefined);
            this.Email(undefined);
            this.Position(undefined);
            this.Firm(undefined);
        }

    };
} 