﻿module OpenTrainingsCMS.Dto {

    export interface ITrainingJsonDto {
        Id: number;
        Title: string;
        Date: string;
        Trainer: string;
        PublishDate: string;
        RangeOfTraining: string;
        Requirements: string;
    }

    export class TrainingDto {        

        public Id: number;
        public Title: KnockoutObservable<string>;
        public Date: KnockoutObservable<Date>;
        public Trainer: KnockoutObservable<string>;
        public PublishDate: KnockoutObservable<Date>;
        public RangeOfTraining: KnockoutObservable<string>;
        public Requirements: KnockoutObservable<string>;

        constructor() {

            this.Id = -1;
            this.Title = ko.observable(undefined);
            this.Date = ko.observable(new Date());
            this.Trainer = ko.observable(undefined);
            this.PublishDate = ko.observable(new Date());
            this.RangeOfTraining = ko.observable(undefined);
            this.Requirements = ko.observable(undefined);
        }        

        update = (source: any): void => {
            this.Id = source.Id;
            this.Title(source.Title);
            this.Date(source.Date instanceof Date ? source.Date : new Date(parseInt(source.Date.replace(/\D/g, ''))));            
            this.Trainer(source.Trainer);
            this.PublishDate(source.PublishDate instanceof Date ? source.PublishDate : new Date(parseInt(source.PublishDate.replace(/\D/g, ''))));
            this.RangeOfTraining(source.RangeOfTraining);
            this.Requirements(source.Requirements);
        }

        parse = (): any => {
            return {
                Id: this.Id,
                Title: this.Title(),
                Date: this.Date().toUTCString(),
                Trainer: this.Trainer(),
                PublishDate: this.PublishDate().toUTCString(),
                RangeOfTraining: this.RangeOfTraining(),
                Requirements: this.Requirements()
            };
        }

    };
} 