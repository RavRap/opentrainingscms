﻿/// <reference path="../typings/_all.ts" />
/// <reference path="dto/trainingdto.ts" />

module OpenTrainingsCMS {        

    export class AdminApp {                

        private _grid: JQueryJqGridStatic;
        private _gridData: Array<any>;
        private _selectedTraining: any;
        private _isRemoveRequested = false;

        public training: Dto.TrainingDto;
        public isAddMode: KnockoutObservable<boolean>;

        constructor() {

            this.training = new Dto.TrainingDto();
            this.isAddMode = ko.observable(true);

            this._selectedTraining = null;

            this.setupGrid();
        }

        setupGrid = (): void => {

            var self = this;

            $("#divTrainings").jqGrid({
                mtype: 'GET',
                ajaxGridOptions: { contentType: "application/json" },
                url: '/Admin/GetGridData', //'/api/Training/GetGridData',
                datatype: "json",
                colNames: ['Title', 'Range Of Training', 'Publish Date', 'Date', 'Requirements', 'Trainer', ''],
                colModel: [                    
                    { name: 'Title', index: 'Title', width: 90, sortable: false },
                    { name: 'RangeOfTraining', index: 'RangeOfTraininge', width: 150, sortable: false },
                    { name: 'PublishDate', index: 'PublishDate', width: 80, align: "right", formatter: "date", sortable: false },
                    { name: 'Date', index: 'Date', width: 80, align: "right", formatter: "date", sortable: false },
                    { name: 'Requirements', index: 'Requirements', width: 150, align: "right", sortable: false },
                    { name: 'Trainer', index: 'Trainer', width: 150, sortable: false },
                    {
                        name: 'Id', index: 'Id', width: 25, sortable: false, formatter: () => {
                            return "<a href='javascript:void(0);' onclick='OpenTrainingsCMS.adminApp.requestRemoval();'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a>";
                        }
                    }
                ],
                rowNum: 10,
                rowList: [5, 10, 25],
                pager: '#pager',
                sortname: 'id',
                viewrecords: true,
                sortorder: "desc",
                caption: "Trainings",
                loadComplete: function (data) {
                    self._gridData = data.rows;
                    self._selectedTraining = null;
                    self.isAddMode(true);
                },
                beforeSelectRow: function (rowid, e) {
                    var $td = $(e.target).closest("td");
                    var id: number = parseInt(<any>$td.closest("tr.jqgrow").attr("id")) - 1;
                    if (self._selectedTraining != self._gridData[id]) self._selectedTraining = self._gridData[id];
                    else self._selectedTraining = null;

                    self.onSelect();

                    if (self._isRemoveRequested) self.remove();

                    //var $self = $(this),
                    //    $td = $(e.target).closest("td"),
                    //    rowid = <any>$td.closest("tr.jqgrow").attr("id"),
                    //    iCol = (<any>$).jgrid.getCellIndex($td[0]),
                    //    cm = $self.jqGrid("getGridParam", "colModel");                                      
                    return true;
                },
                onPaging: function (e) {
                    self._selectedTraining = null;
                },
                onSortCol: function (index, columnIndex, sortOrder) {
                    self._selectedTraining = null;
                }                
            });
        }

        onSelect = (): void => {
            if (this._selectedTraining) {
                this.isAddMode(false);
                this.training.update(this._selectedTraining);
            } else {
                this.isAddMode(true);
            };
        }

        add = (): void => {
            $.ajax({
                type: 'POST',
                url: '/Admin/AddTraining',
                data: { training: this.training.parse() },
                success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                    $("#divTrainings").jqGrid('setGridParam', { datatype: 'json' }).trigger("reloadGrid");
                }
            });
        }

        update = (): void => {
            $.ajax({
                type: 'POST',
                url: '/Admin/UpdateTraining',
                data: { training: this.training.parse() },
                success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                    $("#divTrainings").jqGrid('setGridParam', { datatype: 'json' }).trigger("reloadGrid");
                }
            });
        }

        requestRemoval = (): void => {
            this._isRemoveRequested = true;
        }

        remove = (): void => {
            
            this._isRemoveRequested = false;
            if (this._selectedTraining) {
                $.ajax({
                    type: 'POST',
                    url: '/Admin/RemoveTraining',
                    data: { id: this._selectedTraining.Id },
                    success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                        $("#divTrainings").jqGrid('setGridParam', { datatype: 'json' }).trigger("reloadGrid");
                    }
                });
            }
        }
    };

    export var adminApp = new OpenTrainingsCMS.AdminApp();
    ko.applyBindings(adminApp);

}