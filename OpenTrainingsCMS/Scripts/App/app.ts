﻿/// <reference path="../typings/_all.ts" />

ko.validation.init({
    insertMessages: false,
    decorateElement: true,
    decorateElementOnModified: true,
    errorElementClass: 'error',
    errorClass: 'error',
    messagesOnModified: false,
    decorateInputElement: true
}, true);

module OpenTrainingsCMS {

    class PopupHelper {

        public static MENU_CLASS: string = "navbar";
        public openedFromPosition: number;

        constructor(private _onClose?: Function) {

            this.openedFromPosition = undefined;
        }

        onPopupOpen = (): void => {
            this.openedFromPosition = $(window).scrollTop();
            $('.' + PopupHelper.MENU_CLASS).hide();
        }

        onPopupClose = (): void => {
            if (this._onClose) this._onClose();
            $('.' + PopupHelper.MENU_CLASS).show();
            var vm = this;
            setTimeout(() => {
                $(window).scrollTop(vm.openedFromPosition);
                menu_focus($("li[data-slide=1]"), 1);
            }, 25);
        }
    }

    //http://tympanus.net/codrops/2012/06/05/fullscreen-slit-slider-with-jquery-and-css3/
    class Carousel {

        public items: KnockoutObservableArray<any>;
        private _animations: Array<any>;

        constructor() {

            this.setupAnimations();
            this.setupItems();
        }

        setupItems = (): void => {

            this.items = ko.observableArray();

            var self = this;
            var i = 0;
            var numberPattern = /\d+/g;
            $.each(trainings, (indexInArray: number, valueOfElement: Dto.ITrainingJsonDto) => {

                var numbers : Array<string> = valueOfElement.Date.match(numberPattern);
                valueOfElement.Date = (new Date(parseInt(numbers[0]))).toISOString();
                this.items.push($.extend({}, valueOfElement, self._animations[i]));
                if (i >= self._animations.length - 1) i = 0; else i++;
            });
        }

        setupAnimations = (): void => {
            this._animations = [
                {
                    orientation: 'horizontal',
                    slice1_rotation: -25,
                    slice2_rotation: -25,
                    slice1_scale: 2,
                    slice2_scale: 2,
                    _class: 'bg-img bg-img-1'
                },
                {
                    orientation: 'vertical',
                    slice1_rotation: 10,
                    slice2_rotation: -15,
                    slice1_scale: 1.5,
                    slice2_scale: 1.5,
                    _class: 'bg-img bg-img-2'
                },
                {
                    orientation: 'horizontal',
                    slice1_rotation: 3,
                    slice2_rotation: 3,
                    slice1_scale: 2,
                    slice2_scale: 1,
                    _class: 'bg-img bg-img-3'
                },
                {
                    orientation: 'vertical',
                    slice1_rotation: -5,
                    slice2_rotation: 25,
                    slice1_scale: 2,
                    slice2_scale: 1,
                    _class: 'bg-img bg-img-4'
                },
                {
                    orientation: 'horizontal',
                    slice1_rotation: -5,
                    slice2_rotation: 10,
                    slice1_scale: 2,
                    slice2_scale: 1,
                    _class: 'bg-img bg-img-5'
                }
            ];
        }
    }

    export class App {

        public popupHelper: PopupHelper;
        public carousel: Carousel;
        private _chosenTraining: Dto.TrainingDto;
        public signUpHeader: KnockoutComputed<string>;
        public user: Dto.UserDto;
        public agreedToTerms: KnockoutObservable<boolean>;
        public canSign: KnockoutObservable<boolean>;

        public showErrors: KnockoutObservable<boolean>;
        public validationModel: KnockoutValidationGroup;

        constructor() {

            this.popupHelper = new PopupHelper(() => this.user.reset());
            this.carousel = new Carousel();

            this._chosenTraining = new Dto.TrainingDto();

            this.signUpHeader = ko.computed({
                owner: this,
                read: () => {
                    return 'Training: ' + this._chosenTraining.Title();
                }
            });

            this.user = new Dto.UserDto();
            this.agreedToTerms = ko.observable(false);
            this.canSign = ko.observable(true);

            this.user.Email.subscribe(newValue => {

                var self = this;
                $.ajax({
                    type: 'POST',
                    url: '/Home/GetUser',
                    data: {                        
                        user: this.user.parse()
                    },
                    //contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {
                        if (data !== false) {

                            var matched: Array<any> = $.grep(data.TrainingIds, (elementOfArray: number, indexInArray: number) => {
                                return elementOfArray == self._chosenTraining.Id;
                            });

                            if (matched.length > 0) {
                                // user is already signed to choosen training
                                self.canSign(false);
                            } else {
                                // found user not signed to choosen training, fill other input fields
                                self.canSign(true);
                                self.user.update(data);
                            }
                        } else {
                            self.canSign(true);
                        };
                    },
                    error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {
                    }
                });
            });

            this.setupValidation();
        }

        setupValidation = () => {

            this.user.FirstName.extend({ required: { params: true, message: 'First name is required' } });
            this.user.LastName.extend({ required: { params: true, message: 'Last name is required.' } });
            this.user.Email.extend({ required: { params: true, message: 'Email is required.' } })
                .extend({ email: { params: true, message: 'Email is required.' } });
            this.user.Firm.extend({ required: { params: true, message: 'Firm is required.' } });
            this.user.Phone.extend({ required: { params: true, message: 'Phone is required.' } });
            this.user.Position.extend({ required: { params: true, message: 'Position is required.' } });
            this.agreedToTerms.extend({
                validation: [
                    {
                        validator: value => value === true,
                        message: () => "You must agree to terms."
                    }]
            });

            this.showErrors = ko.observable(false);
            this.validationModel = ko.validatedObservable({
                FirstName: this.user.FirstName,
                LastName: this.user.LastName,
                Email: this.user.Email,
                Firm: this.user.Firm,
                Phone: this.user.Phone,
                Position: this.user.Position,
                AgreedToTerms: this.agreedToTerms
            });
            this.validationModel.errors.showAllMessages(false);
        }

        startSigningUp = (training: Dto.TrainingDto): void => {
            this._chosenTraining.update(training);
            this.showErrors(false);
        }        

        signUp = (): void => {

            this.showErrors(true);
            this.validationModel.errors.showAllMessages(true);
            if (this.validationModel.isValid()) {

                $.ajax({
                    type: 'POST',
                    url: '/Home/SignToTraining',
                    data: {
                        trainingId: this._chosenTraining.Id,
                        user: this.user.parse()
                    },
                    success: (data: any, textStatus: string, jqXHR: JQueryXHR) => {                        
                        $("button.btn.md-close").click(); //not sweet, but well I work here with beautiful plguging with ugly code
                        menu_focus($("li[data-slide=1]"), 1);
                    }
                });

            };
        }
    }

    export var app = new OpenTrainingsCMS.App();
    ko.applyBindings(app);

}