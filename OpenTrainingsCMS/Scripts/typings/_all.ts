﻿/// <reference path="jquery/jquery.d.ts" />
/// <reference path="knockout/knockout.d.ts" /> 
/// <reference path="jqgrid/jqgrid.d.ts" />
/// <reference path="jquery.ui.datetimepicker/jquery.ui.datetimepicker.d.ts" />
/// <reference path="knockout.validation/knockout.validation.d.ts" />

declare module OpenTrainingsCMS {
    var trainings: Array<Dto.TrainingDto>;
}

declare var classie: any;

declare var menu_focus: (element: JQuery, i: number) => void;