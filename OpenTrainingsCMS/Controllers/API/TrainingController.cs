﻿using MvcJqGrid;
using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OpenTrainingsCMS.Controllers.API
{
    public class TrainingController : ApiController
    {
        private readonly ITrainingFinder _trainingFinder;

        public TrainingController(ITrainingFinder trainingFinder)
        {
            _trainingFinder = trainingFinder;
        }

        //[HttpPost]
        public HttpResponseMessage GetGridData([System.Web.Mvc.ModelBinder(typeof(GridModelBinder))]GridSettings grid)
        {
            return Request.CreateResponse<IEnumerable<TrainingDto>>(_trainingFinder.GetByExpiratoion());            
        }
    }
}
