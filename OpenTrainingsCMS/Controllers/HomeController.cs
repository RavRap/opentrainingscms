﻿using Messaging;
using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OpenTrainingsCMS.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITrainingFinder _trainingFinder;
        private readonly IUserFinder _userFinder; 
        private readonly ICommandHandler<SignToTrainingCmd> _signingService;

        public HomeController(ITrainingFinder trainingFinder, IUserFinder userFinder, ICommandHandler<SignToTrainingCmd> signingService)
        {
            _trainingFinder = trainingFinder;
            _userFinder = userFinder;
            _signingService = signingService;
        }

        public ActionResult Index()
        {
            ViewBag.Trainings = _trainingFinder.GetByExpiratoion();

            return View();
        }             

        [HttpPost]
        public void SignToTraining(int trainingId, UserDto user)
        {
            _signingService.Handle(new SignToTrainingCmd { TrainingId = trainingId, User = user });
        }

        [HttpPost]
        public JsonResult GetUser(UserDto user)
        {
            UserDto matchedUser = _userFinder.GetByEmail(user.Email);
            return matchedUser != null ? Json(matchedUser, JsonRequestBehavior.DenyGet) : Json(false, JsonRequestBehavior.DenyGet);
        }
    }
}