﻿using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OpenTrainingsCMS.Controllers
{
    public class AdminController : Controller
    {
        private readonly ITrainingFinder _trainingFinder;        
        private readonly ITrainingRepository _trainingRepository;

        public AdminController(ITrainingFinder trainingFinder, ITrainingRepository trainingRepository)
        {
            _trainingFinder = trainingFinder;   
            _trainingRepository = trainingRepository;
        }

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetGridData(MvcJqGrid.GridSettings grid)
        {
            IQueryable<TrainingDto> data = _trainingFinder.GetByExpiratoion();

            var pagedData = data.OrderByDescending(d => d.Date).Skip(grid.PageSize * (grid.PageIndex - 1)).Take(grid.PageSize);

            var result = new
            {
                total = (int)Math.Ceiling((double)data.Count() / grid.PageSize),
                page = grid.PageIndex,
                records = pagedData.Count(),
                rows = pagedData
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        [HttpPost]
        public void RemoveTraining(int id)
        {
            _trainingRepository.Remove(id);
        }

        [Authorize]
        [HttpPost]
        public void AddTraining(TrainingDto training)
        {
            _trainingRepository.Add(training);
        }

        [Authorize]
        [HttpPost]
        public void UpdateTraining(TrainingDto training)
        {
            _trainingRepository.Updade(training);
        }
    }
}