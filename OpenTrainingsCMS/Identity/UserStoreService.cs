﻿using Microsoft.AspNet.Identity;
using OpenTrainingsCms.DomainGateway;
using OpenTrainingsCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OpenTrainingsCMS.Identity
{
    public class UserStoreService : IUserStore<ApplicationUser>, IUserPasswordStore<ApplicationUser>, IUserLockoutStore<ApplicationUser, string>, IUserTwoFactorStore<ApplicationUser, string>, IUserEmailStore<ApplicationUser>, IUserPhoneNumberStore<ApplicationUser>, IUserLoginStore<ApplicationUser>
    {
        private readonly IUserFinder _userFinder;
        private readonly IUserRepository _userRepository;
        private bool _disposed;

        public UserStoreService(IUserFinder userFinder, IUserRepository userRepository)
        {
            _userFinder = userFinder;
            _userRepository = userRepository;
        }

        public Task CreateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public async Task<ApplicationUser> FindByIdAsync(string userId)
        {
            var task = await _userFinder.GetByIdAsync(int.Parse(userId));
            return task.ToApplicationUser();
        }

        public async Task<ApplicationUser> FindByNameAsync(string userName)
        {
            var task = await _userFinder.GetByEmailAsync(userName);
            return task.ToApplicationUser();
        }

        public Task UpdateAsync(ApplicationUser user)
        {
            return Task.FromResult(false); //empty task
            /*
            var u = _userQueryService.GetUserByNameAsync(user.UserName).Result;
            return Task.Factory.StartNew(() => _updateUserHandler.Handle(user.UserName, 
                new UpdateSiteUserCommand 
                { 
                    SiteUserId = u.SiteUserId,
                    Password = user.PasswordHash
                    //TODO
                }));            
             */
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing) { /* Free any managed objects here */ }

            /* Free any unmanaged objects here */

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Task<string> GetPasswordHashAsync(ApplicationUser user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(ApplicationUser user)
        {
            return Task.FromResult(user.PasswordHash != null);
        }

        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
        {
            UserDto u = _userFinder.GetByEmailAsync(user.Email).Result;
            user.PasswordHash = u.HashedPassword = passwordHash;
            return Task.Factory.StartNew(() => _userRepository.Update(u));
        }

        public Task<int> GetAccessFailedCountAsync(ApplicationUser user)
        {
            return Task.FromResult(0);
        }

        public Task<bool> GetLockoutEnabledAsync(ApplicationUser user)
        {
            return Task.FromResult(false);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(ApplicationUser user)
        {
            return Task.FromResult(new DateTimeOffset(DateTime.Now));
        }

        public Task<int> IncrementAccessFailedCountAsync(ApplicationUser user)
        {
            return Task.FromResult(0);
        }

        public Task ResetAccessFailedCountAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEnabledAsync(ApplicationUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetTwoFactorEnabledAsync(ApplicationUser user)
        {
            return Task.FromResult(false);
        }

        public Task SetTwoFactorEnabledAsync(ApplicationUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        #region IUserEmailStore

        public Task<ApplicationUser> FindByEmailAsync(string email)
        {
            return Task.FromResult(_userFinder.GetByEmail(email).ToApplicationUser());
        }

        public Task<string> GetEmailAsync(ApplicationUser user)
        {
            return Task.FromResult(user.Email);
            //throw new NotImplementedException();
        }

        public Task<bool> GetEmailConfirmedAsync(ApplicationUser user)
        {
            return Task.FromResult(true);
            //throw new NotImplementedException();
        }

        public Task SetEmailAsync(ApplicationUser user, string email)
        {
            UserDto u = _userFinder.GetByEmailAsync(user.UserName).Result;
            user.Email = u.Email = email;
            return Task.Factory.StartNew(() => _userRepository.Update(u));
        }

        public Task SetEmailConfirmedAsync(ApplicationUser user, bool confirmed)
        {
            return Task.FromResult(false);
            //throw new NotImplementedException();
        }

        #endregion IUserEmailStore

        #region IUserPhoneNumberStore

        public Task<string> GetPhoneNumberAsync(ApplicationUser user)
        {
            return Task.FromResult("NOT AVAILABLE");
            //throw new NotImplementedException();
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetPhoneNumberAsync(ApplicationUser user, string phoneNumber)
        {
            throw new NotImplementedException();
        }

        public Task SetPhoneNumberConfirmedAsync(ApplicationUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        #endregion IUserPhoneNumberStore

        #region IUserLoginStore

        public Task AddLoginAsync(ApplicationUser user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationUser> FindAsync(UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task RemoveLoginAsync(ApplicationUser user, UserLoginInfo login)
        {
            throw new NotImplementedException();
        }

        #endregion IUserLoginStore
    }
}