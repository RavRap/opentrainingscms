﻿using OpenTrainingsCms.DomainGateway;
using OpenTrainingsCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OpenTrainingsCMS
{
    public static class UserExtensions
    {
        public static ApplicationUser ToApplicationUser(this UserDto source)
        {
            if (source == null) return null;

            return new ApplicationUser
            {
                Id = source.Id.ToString(),
                Email = source.Email,
                PasswordHash = source.HashedPassword,
                UserName = string.Format("{0} {1}", source.FirstName, source.LastName)
            };
        }
    }
}