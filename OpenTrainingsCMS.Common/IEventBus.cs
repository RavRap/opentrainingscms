﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messaging
{
    public interface IEventBus
    {
        void Send<TEvent>(TEvent message) where TEvent : class;
    }
    public interface IEventPublisher
    {
        void Subscribe<TEvent>(Action<TEvent> action) where TEvent : class;
    }

    public interface IEventHandler<TEvent>
    {
        void Handle(TEvent @event);
    }
}
