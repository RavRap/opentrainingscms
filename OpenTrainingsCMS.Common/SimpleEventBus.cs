﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messaging
{
    public class SimpleEventBus : IEventBus, IEventPublisher
    {
        private readonly Dictionary<Type, List<object>> _subscriptions = new Dictionary<Type, List<object>>();
        public void Send<TEvent>(TEvent message) where TEvent : class
        {


            if (_subscriptions.ContainsKey(typeof(TEvent)))
            {
                var subs = _subscriptions[typeof(TEvent)];
                foreach (var action in subs)
                {
                    (action as Action<TEvent>).Invoke(message);
                }
            }
        }

        public void Subscribe<TEvent>(Action<TEvent> action) where TEvent : class
        {
            var type = typeof(TEvent);
            Subscribe(type, action);
        }

        private void Subscribe<TEvent>(Type type, Action<TEvent> action) where TEvent : class
        {
            if (!_subscriptions.ContainsKey(type))
            {
                _subscriptions.Add(type, new List<object>());
            }

            _subscriptions[type].Add(action);
        }

    }
}