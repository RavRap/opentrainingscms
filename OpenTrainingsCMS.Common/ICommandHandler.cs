﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messaging
{
    public interface ICommandHandler<TCommand> where TCommand : class
    {
        void Handle(TCommand command);
    }

    public interface ICommandHandler<TCommand, TResult> where TCommand : class
    {
        TResult Handle(TCommand command);
    }
}