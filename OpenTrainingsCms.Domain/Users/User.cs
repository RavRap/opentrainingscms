﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.Domain
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string HashedPassword { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Position { get; set; }
        public string Firm { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }

        public ICollection<Training> Trainings { get; set; }

        public User()
        {
            Trainings = new HashSet<Training>();
        }

    }
}