﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.Domain
{
    public class Training
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string RangeOfTraining { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime Date { get; set; }
        public string Requirements { get; set; }
        public string Trainer { get; set; }

        public ICollection<User> Participants { get; set; }

        public Training()
        {
            Participants = new HashSet<User>();
        }
    }
}
