﻿using Messaging;
using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCMS.Application
{
    public interface IEmailCommunicationCmdService : ICommandHandler<PasswordResetRequestCmd>, ICommandHandler<SendEmailOnSigningCmd> { }

    public class EmailCommunicationCmdService : IEmailCommunicationCmdService
    {
        private SmtpClient _smtpClient;
        private IUserFinder _userFinder;
        private ITrainingFinder _trainingFinder;
        private const string ADMIN_EMAIL = "summit_admin@pl.sii.eu";
        private const string SMTP_HOST = "mail.pl.sii.eu";
        private const int SMTP_HOST_PORT = 465; //587;

        public EmailCommunicationCmdService(IUserFinder userFinder, ITrainingFinder trainingFinder)
        {
            _userFinder = userFinder;
            _trainingFinder = trainingFinder;
            SetupSmtpClient();
        }

        public void Handle(PasswordResetRequestCmd command)
        {
            UserDto user = _userFinder.GetById(command.UserId);
            MailMessage mail = new MailMessage(ADMIN_EMAIL, user.Email);
            mail.IsBodyHtml = true;
            mail.Subject = command.Subject;
            mail.Body = command.Body;
            _smtpClient.Send(mail);
        }

        public void Handle(SendEmailOnSigningCmd command)
        {
            TrainingDto training = _trainingFinder.GetByExpiratoion().Single(t => t.Id == command.TrainingId);

            MailMessage mail = new MailMessage(ADMIN_EMAIL, ADMIN_EMAIL);
            mail.IsBodyHtml = true;
            mail.Subject = string.Format("{0} {1} signed for '{2}'", command.User.FirstName, command.User.LastName, training.Title);
            mail.Body = string.Format(@"<p><b>## SIGN UP FOR TRAINING ##</b></p>
<p>Training: <span><b>{0}</b><span> led by: {1}</p>
<p>Participant: {2} {3} (firm: {4}), email: {5}, phone: {6}, position: {7}</p>",
                training.Title, training.Trainer, command.User.FirstName, command.User.LastName, command.User.Firm, command.User.Email, command.User.Phone, command.User.Position);
            _smtpClient.Send(mail);
        }

        private void SetupSmtpClient()
        {
            _smtpClient = new SmtpClient();
            _smtpClient.Host = SMTP_HOST;
            _smtpClient.Port = SMTP_HOST_PORT;
            //_smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            //_smtpClient.UseDefaultCredentials = false;
            _smtpClient.Credentials = new System.Net.NetworkCredential("summit_admin", "Reethaimuz7g");
            _smtpClient.EnableSsl = true;
        }
    }
}
