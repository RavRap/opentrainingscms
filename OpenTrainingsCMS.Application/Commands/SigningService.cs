﻿using Messaging;
using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCMS.Application
{
    public class SigningService : ICommandHandler<SignToTrainingCmd>
    {
        private readonly IUserFinder _userFinder;
        private readonly IUserRepository _userRepository;
        private readonly ITrainingRepository _trainingRepository;
        private readonly ICommandHandler<SendEmailOnSigningCmd> _emailCommunicationCmdService;

        public SigningService(IUserFinder userFinder, IUserRepository userRepository, ITrainingRepository trainingRepository, ICommandHandler<SendEmailOnSigningCmd> emailCommunicationCmdService)
        {
            _userFinder = userFinder;
            _userRepository = userRepository;
            _trainingRepository = trainingRepository;
            _emailCommunicationCmdService = emailCommunicationCmdService;
        }

        public void Handle(SignToTrainingCmd command)
        {
            UserDto user = _userFinder.GetByEmail(command.User.Email);
            if (user == null) {
                _userRepository.Add(command.User);
                user = _userFinder.GetByEmail(command.User.Email);
            };

            if (user.TrainingIds.Any(tId => tId == command.TrainingId)) throw new Exception("Can't sign user to this training.");
            else
            {
                _trainingRepository.AssignUserToTraining(command.TrainingId, user.Id);
                _emailCommunicationCmdService.Handle(new SendEmailOnSigningCmd
                {
                    User = user,
                    TrainingId = command.TrainingId
                });
            }
        }
    }
}