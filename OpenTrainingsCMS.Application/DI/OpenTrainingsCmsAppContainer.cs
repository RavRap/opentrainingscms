﻿using Messaging;
using Microsoft.Practices.Unity;
using OpenTrainingsCms.Domain;
using OpenTrainingsCms.DomainGateway;
using OpenTrainingsCms.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCMS.Application
{
    public class OpenTrainingsCmsAppContainer : UnityContainer, IUnityContainer
    {
        public OpenTrainingsCmsAppContainer()
        {
            //var bus = new SimpleEventBus();

            this.RegisterType<ICommandHandler<SignToTrainingCmd>, SigningService>();

            this.RegisterType<ICommandHandler<PasswordResetRequestCmd>, EmailCommunicationCmdService>();
            this.RegisterType<ICommandHandler<SendEmailOnSigningCmd>, EmailCommunicationCmdService>();            
            this.RegisterType<IEmailCommunicationCmdService, EmailCommunicationCmdService>();

            this.RegisterType<ITrainingFinder, TrainingFinder>();
            this.RegisterType<IUserFinder, UserFinder>();
            //this.RegisterType<ITrainingQueryService, TrainingQueryService>();

            this.RegisterType<IUserRepository, UserRepository>();
            this.RegisterType<ITrainingRepository, TrainingRepository>();

            //this.RegisterInstance<IEventBus>(bus);

            //bus.Subscribe<RawMaterialAdded, RawMaterialChangeLogger>(this);
            //bus.Subscribe<RawMaterialAdded, RawMaterialEventHandler>(this);

        }


    }

    static class Extensions
    {
        public static void Subscribe<TEvent, THandler>(this IEventPublisher bus, UnityContainer container)
            where THandler : IEventHandler<TEvent>
            where TEvent : class
        {
            bus.Subscribe<TEvent>(e => container.Resolve<THandler>().Handle(e));
        }
    }
}
