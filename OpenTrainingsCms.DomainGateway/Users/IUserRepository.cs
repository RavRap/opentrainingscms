﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.DomainGateway
{
    public interface IUserRepository
    {
        void Update(UserDto user);
        void Add(UserDto user);
    }
}