﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.DomainGateway
{
    public interface IUserFinder
    {
        UserDto GetById(int id);
        Task<UserDto> GetByIdAsync(int id);

        UserDto GetByEmail(string email);
        Task<UserDto> GetByEmailAsync(string email);
    }
}