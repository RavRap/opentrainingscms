﻿using Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.DomainGateway.Services
{
    public interface ISigningService : ICommandHandler<SignToTrainingCmd> { }
}