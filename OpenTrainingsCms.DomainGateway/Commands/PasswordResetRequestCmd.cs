﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.DomainGateway
{
    public class PasswordResetRequestCmd
    {
        public int UserId { get; set; }        
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}