﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.DomainGateway
{
    public class SendEmailOnSigningCmd
    {
        public int TrainingId { get; set; }
        public UserDto User { get; set; }
    }
}
