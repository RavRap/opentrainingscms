﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.DomainGateway
{
    public interface ITrainingFinder
    {
        IQueryable<TrainingDto> GetByExpiratoion(bool notExpired = false);
    }
}
