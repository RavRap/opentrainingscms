﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.DomainGateway
{
    public interface ITrainingRepository
    {
        void Add(TrainingDto dto);
        void Updade(TrainingDto dto);
        void Remove(int id);
        void AssignUserToTraining(int trainingId, int userId);
    }
}
