﻿using OpenTrainingsCms.Domain;
using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.Infrastructure
{
    public class TrainingRepository : ITrainingRepository
    {
        private readonly OpenTrainingsCmsDbContext _context;

        public TrainingRepository()
        {
            _context = new OpenTrainingsCmsDbContext();
        }

        public void Add(TrainingDto dto)
        {
            _context.Trainings.Add(dto.FromDto());
            _context.SaveChanges();

        }

        public void Updade(TrainingDto dto)
        {
            Training training = _context.Trainings.Single(t => t.Id == dto.Id);

            training.Date = dto.Date;
            training.PublishDate = dto.PublishDate;
            training.RangeOfTraining = dto.RangeOfTraining;
            training.Requirements = dto.Requirements;
            training.Title = dto.Title;
            training.Trainer = dto.Trainer;

            _context.SaveChanges();
        }

        public void Remove(int id)
        {
            Training training = _context.Trainings.Single(t => t.Id == id);

            _context.Trainings.Remove(training);

            _context.SaveChanges();
        }

        public void AssignUserToTraining(int trainingId, int userId)
        {
            Training training = _context.Trainings.Single(t => t.Id == trainingId);
            User user = _context.Users.Single(u => u.Id == userId);
            training.Participants.Add(user);

            _context.SaveChanges();
        }
    }
}
