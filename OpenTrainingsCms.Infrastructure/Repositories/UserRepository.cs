﻿using OpenTrainingsCms.Domain;
using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.Infrastructure
{
    public class UserRepository : IUserRepository
    {
        private readonly OpenTrainingsCmsDbContext _context;

        public UserRepository()
        {
            _context = new OpenTrainingsCmsDbContext();
        }

        public void Update(UserDto user)
        {
            User dUser = _context.Users.FirstOrDefault(u => u.Id == user.Id);

            dUser.Email = user.Email;
            dUser.HashedPassword = user.HashedPassword;
            dUser.FirstName = user.FirstName;

            _context.SaveChanges();
        }

        public void Add(UserDto user)
        {
            _context.Users.Add(user.FromDto());

            _context.SaveChanges();
        }
    }
}
