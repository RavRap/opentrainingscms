﻿using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.Infrastructure
{
    public class TrainingFinder : ITrainingFinder
    {
        private readonly OpenTrainingsCmsDbContext _context;

        public TrainingFinder()
        {
            _context = new OpenTrainingsCmsDbContext();
        }

        public IQueryable<TrainingDto> GetByExpiratoion(bool notExpired = false)
        {
            return _context.Trainings.Where(t => notExpired ? t.PublishDate <= DateTime.Now && t.Date >= DateTime.Now : true).ToDto();
        }
    }
}
