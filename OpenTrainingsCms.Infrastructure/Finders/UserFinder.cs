﻿using OpenTrainingsCms.Domain;
using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.Infrastructure
{
    public class UserFinder : IUserFinder
    {
        private readonly OpenTrainingsCmsDbContext _context;

        public UserFinder()
        {
            _context = new OpenTrainingsCmsDbContext();
        }

        public UserDto GetById(int id)
        {
            User user = _context.Users.Include("Trainings").FirstOrDefault(u => u.Id == id);
            return user != null ? user.ToDto() : null;
        }

        public Task<UserDto> GetByIdAsync(int id)
        {
            User user = _context.Users.Include("Trainings").FirstOrDefault(u => u.Id == id);
            return Task.FromResult(user != null ? user.ToDto() : null);
        }

        public UserDto GetByEmail(string email)
        {
            User user = _context.Users.Include("Trainings").FirstOrDefault(u => u.Email.ToLower() == email.ToLower());
            return user != null ? user.ToDto() : null;
        }

        public Task<UserDto> GetByEmailAsync(string email)
        {
            User user = _context.Users.Include("Trainings").FirstOrDefault(u => u.Email.ToLower() == email.ToLower());
            return Task.FromResult(user != null ? user.ToDto() : null);
        }
    }
}
