﻿using OpenTrainingsCms.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.Infrastructure
{
    public class OpenTrainingsCmsDbInitializer : DropCreateDatabaseIfModelChanges<OpenTrainingsCmsDbContext>
    {
        protected override void Seed(OpenTrainingsCmsDbContext context)
        {
            Enumerable.Range(0, 10).ToList().ForEach(i =>
            {
                context.Trainings.Add(new Training { 
                    Title = "Description" + i,
                    PublishDate = DateTime.Now.AddDays(-i),
                    Date = DateTime.Now.AddDays(i)
                });
            });

            context.Users.Add(new User
            {
                Email = "awegrzanowska@pl.sii.eu",
                FirstName = "Anna",
                LastName = "Węgrzanowska",
                HashedPassword = "1000:IuZZjtN6h399msPe971l+eHQK1dw5hnO:0E2XY8sNZgLkBRLlqb62Rwrht41LcOso",
                IsAdmin = true
            });

            context.SaveChanges();

            base.Seed(context);
        }
    }
}
