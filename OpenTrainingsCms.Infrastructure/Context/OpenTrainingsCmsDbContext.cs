﻿using OpenTrainingsCms.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.Infrastructure
{
    public class OpenTrainingsCmsDbContext : DbContext
    {
        public DbSet<Training> Trainings { get; set; }
        public DbSet<User> Users { get; set; }

        public OpenTrainingsCmsDbContext()
        {
            Database.SetInitializer(new OpenTrainingsCmsDbInitializer());

            base.Database.Log = s => Debug.WriteLine(s);
        }

        //hack for copying EF dll to build folder
        private static SqlProviderServices _instance = SqlProviderServices.Instance;

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(this.GetType().Assembly);
        }
    }
}
