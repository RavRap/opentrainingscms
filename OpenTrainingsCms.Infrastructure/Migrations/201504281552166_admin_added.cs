namespace OpenTrainingsCms.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class admin_added : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        HashedPassword = c.String(),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            Sql("INSERT INTO dbo.Users (Email, HashedPassword, IsActive) VALUES ('rrapcewicz@pl.sii.eu', '1000:O8h3V40khJxcFKkbycyNW5ZvwzPHrxBa:gqsJQjAdx8xU/83f64JVkdO+5waXD0Th', 1);");

        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
        }
    }
}
