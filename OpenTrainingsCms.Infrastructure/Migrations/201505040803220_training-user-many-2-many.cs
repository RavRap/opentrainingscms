namespace OpenTrainingsCms.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class trainingusermany2many : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserTrainings",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Training_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Training_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Trainings", t => t.Training_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Training_Id);
            
            AddColumn("dbo.Users", "FirstName", c => c.String());
            AddColumn("dbo.Users", "LastName", c => c.String());
            AddColumn("dbo.Users", "Phone", c => c.String());
            AddColumn("dbo.Users", "Position", c => c.String());
            AddColumn("dbo.Users", "Firm", c => c.String());
            AddColumn("dbo.Users", "IsAdmin", c => c.Boolean(nullable: false));
            DropColumn("dbo.Users", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Name", c => c.String());
            DropForeignKey("dbo.UserTrainings", "Training_Id", "dbo.Trainings");
            DropForeignKey("dbo.UserTrainings", "User_Id", "dbo.Users");
            DropIndex("dbo.UserTrainings", new[] { "Training_Id" });
            DropIndex("dbo.UserTrainings", new[] { "User_Id" });
            DropColumn("dbo.Users", "IsAdmin");
            DropColumn("dbo.Users", "Firm");
            DropColumn("dbo.Users", "Position");
            DropColumn("dbo.Users", "Phone");
            DropColumn("dbo.Users", "LastName");
            DropColumn("dbo.Users", "FirstName");
            DropTable("dbo.UserTrainings");
        }
    }
}
