namespace OpenTrainingsCms.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Trainings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        RangeOfTraining = c.String(),
                        PublishDate = c.DateTime(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Requirements = c.String(),
                        Trainer = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Trainings");
        }
    }
}
