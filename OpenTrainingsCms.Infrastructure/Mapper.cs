﻿using OpenTrainingsCms.Domain;
using OpenTrainingsCms.DomainGateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTrainingsCms.Infrastructure
{
    public static class Mapper
    {
        public static IQueryable<TrainingDto> ToDto(this IQueryable<Training> source)
        {
            return source.Select(t => new TrainingDto
            {
                Id = t.Id,
                Date = t.Date,
                PublishDate = t.PublishDate,
                RangeOfTraining = t.RangeOfTraining,
                Requirements = t.Requirements,
                Title = t.Title,
                Trainer = t.Trainer
            });
        }

        public static Training FromDto(this TrainingDto source)
        {
            return new Training
            {
                Date = source.Date,
                PublishDate = source.PublishDate,
                RangeOfTraining = source.RangeOfTraining,
                Requirements = source.Requirements,
                Title = source.Title,
                Trainer = source.Trainer
            };
        }

        public static UserDto ToDto(this User source)
        {
            return new UserDto
            {
                Id = source.Id,
                Email = source.Email,
                HashedPassword = source.HashedPassword,
                FirstName = source.FirstName,
                LastName = source.LastName,
                Firm = source.Firm,
                IsAdmin = source.IsAdmin,
                Phone = source.Phone,
                Position = source.Position,
                TrainingIds = source.Trainings.Select(t => t.Id)
            };
        }

        public static User FromDto(this UserDto source)
        {
            return new User
            {
                Email = source.Email,
                Firm = source.Firm,
                FirstName = source.FirstName,
                HashedPassword = source.HashedPassword,
                IsAdmin = source.IsAdmin,
                LastName = source.LastName,
                Phone = source.Phone,
                Position = source.Position                
            };
        }
    }
}
